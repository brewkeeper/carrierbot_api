module CarrierBotAPI
  class Item
    attr_reader :name, :sku, :quantity, :grams, :price, :vendor, :requires_shipping, :taxable, :fulfillment_service, :properties, :product_id, :variant_id

    def initialize(data)
      @name = data[:name]
      @sku = data[:sku]
      @quantity = data[:quantity]
      @grams = data[:grams]
      @price = data[:price]
      @vendor = data[:vendor] || CarrierBotAPI::Configuration['carrierbot_vendor']
      @requires_shipping = data[:requires_shipping]
      @taxable = data[:taxable]
      @fulfillment_service = data[:fulfillment_service] || CarrierBotAPI::Configuration['carrierbot_fulfillment_service'] || 'manual'
      @properties = data[:properties]
      @product_id = data[:product_id]
      @variant_id = data[:variant_id]
    end

    def to_json
      {
        'name': self.name,
        'sku': self.sku,
        'quantity': self.quantity,
        'grams': self.grams,
        'price': self.price,
        'vendor': self.vendor,
        'requires_shipping': self.requires_shipping,
        'taxable': self.taxable,
        'fulfillment_service': self.fulfillment_service,
        'properties': self.properties,
        'product_id': self.product_id,
        'variant_id': self.variant_id
      }
    end
  end
end # module CarrierBotAPI
