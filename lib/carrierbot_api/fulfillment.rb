module CarrierBotAPI
  class Fulfillment
    attr_reader :ordered_at, :fulfilled_at, :total_price, :service_code, :shipping_code, :order_id, :fulfillment_id, :destination, :items

    def initialize(data)
      @ordered_at = data[:ordered_at]
      @fulfilled_at = data[:fulfilled_at]
      @total_price = data[:total_price]
      @shipping_code = data[:shipping_code]
      @service_code = data[:service_code]
      @order_id = data[:order_id]
      @fulfillment_id = data[:fulfillment_id]
      @destination = data[:destination]
      @items = data[:items]
    end

    def to_json
      {
        'ordered_at': self.ordered_at,
        'fulfilled_at': self.fulfilled_at,
        'total_price': self.total_price,
        'service_code': self.service_code,
        'shipping_code': self.shipping_code,
        'order_id': self.order_id,
        'fulfillment_id': self.fulfillment_id,
        'destination': self.destination&.to_json,
        'items': self.items.map(&:to_json)
      }
    end
  end
end # module CarrierBotAPI
