module CarrierBotAPI
  class RatesRequest
    attr_accessor :url

    def initialize(url = nil)
      @url = url || Setting.current&.carrier_service_url

      origin_country = CarrierBotAPI::Configuration['carrierbot_origin_country']
      origin_postal_code = CarrierBotAPI::Configuration['carrierbot_origin_postal_code']
      origin_province = CarrierBotAPI::Configuration['carrierbot_origin_province']
      origin_city = CarrierBotAPI::Configuration['carrierbot_origin_city']
      origin_name = CarrierBotAPI::Configuration['carrierbot_origin_name']
      origin_address1 = CarrierBotAPI::Configuration['carrierbot_origin_address1']
      origin_address2 = CarrierBotAPI::Configuration['carrierbot_origin_address2']
      origin_address3 = CarrierBotAPI::Configuration['carrierbot_origin_address3']
      origin_phone = CarrierBotAPI::Configuration['carrierbot_origin_phone']
      origin_fax = CarrierBotAPI::Configuration['carrierbot_origin_fax']
      origin_email = CarrierBotAPI::Configuration['carrierbot_origin_email']
      origin_address_type = CarrierBotAPI::Configuration['carrierbot_origin_address_type']
      origin_company_name = CarrierBotAPI::Configuration['carrierbot_origin_company_name']

      @origin = Address.new(
        country: origin_country,
        postal_code: origin_postal_code,
        province: origin_province,
        city: origin_city,
        name: origin_name,
        address1: origin_address1,
        address2: origin_address2,
        address3: origin_address3,
        phone: origin_phone,
        fax: origin_fax,
        email: origin_email,
        address_type: origin_address_type,
        company_name: origin_company_name)
    end

    def get(destination: nil, items: [], fulfillments: [])
      request = {
        'rate': {
          'origin': @origin.to_json,
          'destination': destination&.to_json,
          'items': items.map(&:to_json),
          'fulfillments': fulfillments.map(&:to_json),
          'currency': 'JPY', 'locale': 'en'
        }
      }

      get_raw(request)
    end

    def get_raw(request)
      conn = Faraday.new(url: @url)
      response = conn.post @url, request
      raise "could not retrieve shipping rates - #{response.reason_phrase}\nrequest: #{request}" \
        unless response.success?

      JSON.parse(response.body)['rates']
    end

    def batch_get(fulfillments: [])
      conn = Faraday.new(url: @url)

      request = {
          'origin': @origin.to_json,
          'fulfillments': fulfillments.map(&:to_json),
          'currency': 'JPY', 'locale': 'en'
      }

      response = conn.post '/api/v1/batch_requests', request
      raise "could not retrieve shipping rates - #{response.reason_phrase}\nrequest: #{request}" \
        unless response.success?

      JSON.parse(response.body)
    end

  end
end
