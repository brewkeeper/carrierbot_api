module CarrierBotAPI
  module Configuration
    @defaults = {}
    @config = nil

    class << self
      def load(options={})
        filename = options[:file] || File.join(Rails.root, 'config', 'configuration.yml')
        env = options[:env] || Rails.env

        @config = @defaults.dup
        if File.file?(filename)
          @config.merge!(load_from_yaml(filename, env))
        end
        @config
      end

      def [](name)
        load unless @config
        @config[name]
      end

      private

      def load_from_yaml(filename, env)
        yaml = nil
        begin
          yaml = YAML::load(ERB.new(File.read(filename)).result)
        rescue ArgumentError
          abort "could not load YAML file #{filename}"
        rescue SyntaxError => e
          abort "could not load YAML file #{filename} - syntax error:\n#{e.message}"
        end
        conf = {}
        if yaml.is_a?(Hash)
          if yaml['default']
            conf.merge!(yaml['default'])
          end
          if yaml[env]
            conf.merge!(yaml[env])
          end
        else
          abort "could not load YAML configuration #{filename} - unexpected formatting"
        end
        conf
      end
    end # class methods
  end # module Configuration
end