module CarrierBotAPI
  class Address
    attr_accessor :country, :postal_code, :province, :city, :name, :address1, :address2, :address3, :phone, :fax, :email, :address_type, :company_name

    def initialize(data)
      @country = data[:country]
      @postal_code = data[:postal_code]
      @province = data[:province]
      @city = data[:city]
      @name = data[:name]
      @address1 = data[:address1]
      @address2 = data[:address2]
      @address3 = data[:address3]
      @phone = data[:phone]
      @fax = data[:fax]
      @email = data[:email]
      @address_type = data[:address_type]
      @company_name = data[:company_name]
    end

    def to_json
      {
        'country': self.country, 'postal_code': self.postal_code, 'province': self.province, 'city': self.city, 'name': self.name,
        'address1': self.address1, 'address2': self.address2, 'address3': self.address3,
        'phone': self.phone, 'fax': self.fax, 'email': self.email, 'address_type': self.address_type,
        'company_name': self.company_name
      }
    end
  end
end # module CarrierBot
