require "carrierbot_api/version"

require 'carrierbot_api/configuration'
require 'carrierbot_api/address'
require 'carrierbot_api/item'
require 'carrierbot_api/fulfillment'
require 'carrierbot_api/rates_request'