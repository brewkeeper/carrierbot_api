# CarrierBotAPI

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/carrierbot_api`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'carrierbot_api'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install carrierbot_api

## Usage

A `configuration.yml` file is required in your `/config` directory
```
default:
  carrierbot_origin_country: AU
  carrierbot_origin_postal_code: 6000
  carrierbot_origin_province: WA
  carrierbot_origin_city: Perth
  carrierbot_origin_name: 
  carrierbot_origin_address1: 
  carrierbot_origin_address2: 
  carrierbot_origin_address3: 
  carrierbot_origin_phone: 
  carrierbot_origin_fax: 
  carrierbot_origin_email: 
  carrierbot_origin_address_type: 
  carrierbot_origin_company_name: Seshbot
  carrierbot_vendor: BrewKeeper
  carrierbot_fulfillment_service: manual
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/carrierbot_api. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Code of Conduct

Everyone interacting in the CarrierbotRates project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/carrierbot_api/blob/master/CODE_OF_CONDUCT.md).
