require "test_helper"
class CarrierBotAPITest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::CarrierBotAPI::VERSION
  end

  def test_it_creates_an_address
    address = CarrierBotAPI::Address.new(city: "Australia")
    refute_nil address
  end
end
